Ipoptlib

Purpose

The goal of this project is to provide binary versions of the ipopt library. 
This will allow to manage the ipopt module for Scilab more easily.

TODO 
 * create the Visual project for Ipopt.
 * generate the binary version of mumps on Linux
 * generate the binary version of ipopt on Linux
 * generate the binary version of mumps on Windows
 * generate the binary version of ipopt on Windows

Author

2010 - DIGITEO - Michael Baudin
2010 - DIGITEO - Yann Collette

Licence

This toolbox is distributed under the GNU LGPL license.

Acknowledgements

Michael Baudin thanks Jean-Yves L'Excellent for the help he 
provided during the compilation of Mumps.

